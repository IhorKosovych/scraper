# Yelp Scraper

This is a Scrapy project to scrape info about business from https://www.yelp.com.

### Extracted data
This project extracts different fields like - business name, main image, business description, reviews, etc. The extracted data looks like this sample:

```python
{
    "about_the_business": "Chubby Noodle features hip...",
    "amount_of_review": 592,
    "average_rating": 4,
    "business_id": "yrKFfnMSXTRuq-hkTGM9eA",
    ...
}
```
### Spiders
This project contains one spider and you can list it using the list command:
```python
$ scrapy list
yelp
```
### Arguments

The arguments that spider are waiting for are:
* link for yelp business as argument(**is required**);
* custom name of json file(**not required**). 

### Running the spiders
You can run a spider using the ```scrapy crawl``` command with arguments, such as:

```python 
$ scrapy crawl yelp -a link="https://www.yelp.com/biz/fog-harbor-fish-house-san-francisco-2" -a json_name="file.json"
```
