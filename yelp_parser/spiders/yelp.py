import json

import scrapy

from yelp_parser.items import YelpParserItem


class YelpParser(scrapy.Spider):
    name = 'yelp'
    custom_url = ''
    custom_json_name = ''


    def __init__(self, **kwargs):
        self.custom_url = kwargs.get('link')
        self.custom_json_name = kwargs.get('json_name', 'file.json')

    def start_requests(self):
        yield scrapy.Request(url=self.custom_url, callback=self.parse)
    
    def parse(self, response):
        
        item = YelpParserItem()

        json_text = response.xpath('//*[@id="wrap"]/div[3]/div/script[1]/text()').get()
        obj = json.loads(json_text)
        
        name = obj.get('name')
        item['name'] = name
        
        amount_of_review = obj.get("aggregateRating", {}).get("reviewCount")
        item['amount_of_review'] = amount_of_review

        average_rating = obj.get("aggregateRating", {}).get("ratingValue")
        item['average_rating'] = average_rating

        link = response.url
        item['link'] = link

        main_image = obj.get("image")
        item['main_image'] = main_image

        telephone = obj.get("telephone")
        item['telephone'] = telephone

        street_address = obj.get("address", {}).get('streetAddress')
        item['street_address'] = street_address

        town = obj.get("address", {}).get('addressLocality')
        item['town'] = town
        
        state = obj.get("address", {}).get('addressRegion')
        item['state'] = state

        country = obj.get("address", {}).get('addressCountry')
        item['country'] = country

        post_code = obj.get("address", {}).get('postalCode')
        item['post_code'] = post_code
        
        website = response.xpath('//*[@id="wrap"]/div[3]/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/section[2]/div/div[1]/div/div[2]/p[2]/a/text()').get()
        item['website'] = website
        
        categories = response.xpath('//*[@id="wrap"]/div[3]/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[1]/div[1]/div/div/span[2]/span/a/text()').getall()
        item['categories'] = categories
        
        days = response.xpath('//*[@id="wrap"]/div[3]/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[1]/section[4]/div[2]/div[2]/div/div/table/tbody/tr/th/p/text()').getall()
        hours = response.xpath('//*[@id="wrap"]/div[3]/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[1]/section[4]/div[2]/div[2]/div/div/table/tbody/tr/td[1]/ul/li/p/text()').getall()
        
        working_hours = dict(zip(days, hours))

        item['working_hours'] = working_hours

        json_result = response.xpath('//*[@id="wrap"]/div[3]/script[1]/text()').get()

        amenities = response.xpath('//*[@id="wrap"]/div[3]/div/div[4]/div/div/div[2]/div/div/div[1]/div/div[1]/section[5]/div[2]/div/div/div/div/div/div/span/text()').getall()
        item['amenities'] = amenities
        
        # Some fields are located in the another script.
        obj1 = json.loads(json_result[4:-3])
        
        about_the_business = obj1.get('bizDetailsPageProps', {}).get('fromTheBusinessProps', {}).get('fromTheBusinessContentProps',{}).get('specialtiesText', {})
        item['about_the_business'] = about_the_business
        
        business_id = obj1.get('gaConfig', {}).get('dimensions').get('www', {}).get('business_id')
        # The id of business is located as the second element of the list
        item['business_id'] = business_id[1]

        yield item
