# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class YelpParserItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    name = scrapy.Field()
    link = scrapy.Field()
    amount_of_review = scrapy.Field()
    average_rating = scrapy.Field()
    main_image = scrapy.Field()
    telephone = scrapy.Field()
    post_code = scrapy.Field()
    town = scrapy.Field()
    state = scrapy.Field()
    country = scrapy.Field()
    website = scrapy.Field()
    categories = scrapy.Field()
    working_hours = scrapy.Field()
    about_the_business = scrapy.Field()
    street_address = scrapy.Field()
    amenities = scrapy.Field()
    business_id = scrapy.Field()

